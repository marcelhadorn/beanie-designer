/// <reference path="_references.ts" />

module beanie {

  export class Bootstrap {
    private static _instance: Bootstrap;

    constructor() {
      if (Bootstrap._instance) {
        throw new Error("Error: Instantiation failed: Use beanie.init.App.getInstance() instead of new.");
      }
      Bootstrap._instance = this;
    }


    public static getInstance(): Bootstrap {
      if (Bootstrap._instance == null) {
        Bootstrap._instance = new Bootstrap();
      }
      return Bootstrap._instance;
    }

    public registerAsSharedInstance(app: beanie.App, tagName: string, templateName: string, viewModelClass: any, namespace: string = "") {
      var that = this;
      return function() {
        var component: any = {};

        ko.components.register(tagName, {
          viewModel: {
            createViewModel: function(params, componentInfo) {

              if (!params) {
                params = {};
              }
              params.app = app;
              params.name = tagName; //for backward compatibility
              params.tagName = tagName;
              params.namespace = namespace;
              params.templateName = templateName;
              params.metaData = this.metaData;

              var componentKey = params.tagName + params.name + params.templateName; // that.getComponentKey(params);

              // check if component not already defined or with other params
              if (!component[componentKey]) {
                component[componentKey] = that.newViewModel(params, viewModelClass);
              }
              params.componentInfo = componentInfo;
              return component[componentKey];
            }
          },
          template: {
            element: templateName
          },
          synchronous: true
        })

      }
    }

    public registerComponent(tagName: string, templateName: string, vmInstance: any) {
      ko.components.register(tagName, {
        viewModel: { instance: vmInstance },
        template: {
          element: templateName
        }
      })
    }


    private newViewModel(params, viewModelClass: any) {

      return new viewModelClass(params)

    }

    public bootstrap(metaData: any): void {
      throw new Error('This method is abstract');
    }


    private getComponentKey(params) {

      var key = '';

      for (var property in params) {

        if (params.hasOwnProperty(property) && property !== '$raw') {

          if (typeof params[property] === 'object') {

            key += property + this.getComponentKey(params[property]);

          } else if (typeof params[property] === 'function') {

            key += property + params[property]();

          } else {

            key += property + params[property];

          }

        }
      }

      return key;

    }


  }
}
