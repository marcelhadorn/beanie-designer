/// <reference path="_references.ts" />
declare var ShopifyApp;
module beanie.init {

    export class App extends beanie.Bootstrap {

        public metaData: any;
        app: beanie.App;

        public bootstrap(app: beanie.App): void {
            this.app = app;
            this.registerElements(app);
            this.app.isMobile = this.isMobile();
        }
        isMobile(): boolean {
            if (navigator.userAgent.match(/Android/i)
                || navigator.userAgent.match(/webOS/i)
                || navigator.userAgent.match(/iPhone/i)
                || navigator.userAgent.match(/iPad/i)
                || navigator.userAgent.match(/iPod/i)
                || navigator.userAgent.match(/BlackBerry/i)
                || navigator.userAgent.match(/Windows Phone/i)
                ) {
                return true;
            }
            else {
                return false;
            }
        }

        registerElements(app: beanie.App): void {
            this.registerAsSharedInstance(app, "main", "main", beanie.widgets.Main)();
            this.registerAsSharedInstance(app, "colors", "colors", beanie.widgets.Colors)();
            this.registerAsSharedInstance(app, "layout", "layout", beanie.widgets.Layout)();
            this.registerAsSharedInstance(app, "save", "save", beanie.widgets.Save)();
            this.registerAsSharedInstance(app, "unsupported", "unsupported", beanie.widgets.Unsupported)();
        }
    }
}
