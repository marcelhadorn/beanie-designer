/// <reference path="_references.ts" />

module beanie {

  export class Widget {
    visible: KnockoutObservable<boolean> = ko.observable<boolean>(false);
    hash: KnockoutObservable<any> = ko.observable<any>();
    name: string;
    tagName: string;
    templateName: string;
    element: HTMLElement;
    app: beanie.App;
    constructor(params) {
      this.app = params.app;
      this.tagName = params.tagName;
      this.name = params.name;

    }

    //c[this.app.hash()[0]]

    afterApplyBindings(params: any): void {
        //this.element = p.componentInfo.element;
    }

  }
}
