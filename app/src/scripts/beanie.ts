/// <reference path="_references.ts" />

declare var $;

module beanie {

  export class App {
    hash: KnockoutObservableArray<any> = ko.observableArray<any>();
    data: KnockoutObservable<any> = ko.observable<any>();
    isMobile: boolean;
    constructor(data) {
      this.data(data);
      this.watchLocation();
      console.info('beanieApp started');
      setTimeout(()=>{
        this.hashHasChanged();
      }, 20)
    }

    checkStorage() {
      try {
        var testObject = { 'one': 1, 'two': 2, 'three': 3 };

        // Put the object into storage
        localStorage.setItem('testObject', JSON.stringify(testObject));

        // Retrieve the object from storage
        var retrievedObject = localStorage.getItem('testObject');

      } catch(e) {
        console.error(e);
        ko.postbox.publish("unsupported", '');
      }

    }

    watchLocation(): void {
      var that = this;
      function bindHashChange() {
        that.hashHasChanged();
      }
      window.addEventListener("hashchange", bindHashChange, false);
    }

    hashHasChanged():void {
      var hash = location.hash.substring(1).split('/');
      if(hash.length === 1 && hash[0] === ""){
        hash = [];
      }
      ko.postbox.publish("hide:all", "");
      if(hash.length === 0){
        ko.postbox.publish("main", "");
      } else if(hash.length ===1){
        ko.postbox.publish("layout", hash);
      } else if(hash.length ===2){
        if(hash[1] === 'save') {
          ko.postbox.publish("save", hash);
        } else {
          ko.postbox.publish("color", hash);
        }
      }
    }
  }
}
