/// <reference path="_references.ts" />

declare var svg4everybody;

 svg4everybody({
//     nosvg: false, // shiv <svg> and <use> elements and use image fallbacks
     polyfill: true // polyfill <use> elements for External Content
 });

document.addEventListener('DOMContentLoaded', function() {

    function init(data) {
      var app = new beanie.App(data);
      var beanieApp = new beanie.init.App();
      beanieApp.bootstrap(app);

      var elem: HTMLElement = document.getElementById('m');
      ko.applyBindings(beanieApp, elem);
    }

    $.getJSON( "../data.json", ( data ) => {
      console.info('data json loaded', data);
      if(data){
        init(data);
      }else {
        throw new Error('data not found');
      }
    });

}, false);
