/// <reference path="../_references.ts" />

module beanie.widgets {

  export class Layout extends beanie.Widget {
    title: KnockoutObservable<string> = ko.observable<string>('');
    variants: KnockoutObservableArray<any> = ko.observableArray<any>();
    items: KnockoutObservableArray<any> = ko.observableArray<any>();
    hash: KnockoutObservable<string> = ko.observable<string>('');
    constructor(p) {
      super(p);
        this.title('Choose a pattern');
        this.subscribtions();
    }

    subscribtions(){
      ko.postbox.subscribe("hide:all", () => {
        this.visible(false);
      });
      ko.postbox.subscribe("layout", (d)=>{

        this.hashHasChanged(d);
        this.visible(true);
      });
    }

    getVariants(hash):any{
      try {
        var d = this.app.data();
        var v = d.beanies[hash];
        return v;
      }
      catch(e){
          return null;
      }
    }

    hashHasChanged(d){
        this.hash(d);
        var variation = [];
        var variants = this.getVariants(d);
        console.warn(' Layout hashHasChanged', variation, variants);
        for(var variant in variants.variations){
          variation.push(variant);
        }
        this.items(variants.variations);
        this.variants(variation);
    }
  }
}
