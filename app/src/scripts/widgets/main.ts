/// <reference path="../_references.ts" />

module beanie.widgets {

  export class Main extends beanie.Widget {
    title: KnockoutObservable<string> = ko.observable<string>('');
    beanies: KnockoutObservableArray<any> = ko.observableArray<any>();
    items: KnockoutObservable<any> = ko.observable<any>();
    constructor(p) {
      super(p);
        this.setState();
    }

    setState(){
      ko.postbox.subscribe("hide:all", () => {
        this.visible(false);
      });
      ko.postbox.subscribe("main", () => {
        this.visible(true);
        this.getData();
      });
    }
    getData():void {
      var d = this.app.data();
      try {
        var _beanies = d.beanies;
        this.title(d.title);
        var beanies = [];
        for( var beanie in _beanies){
          beanies.push(beanie);
        }
        this.items(_beanies);
        this.beanies(beanies);
      }
      catch(e) {
          throw Error('Could not read beanies');
      }
    }
  }
}
