/// <reference path="../_references.ts" />

module beanie.widgets {

  export class Colors extends beanie.Widget {
    title: KnockoutObservable<string> = ko.observable<string>('');
    layout: KnockoutObservable<string> = ko.observable<string>('');
    pattern: KnockoutObservable<string> = ko.observable<string>('');
    mask: KnockoutObservable<string> = ko.observable<string>('');
    bobble: KnockoutObservable<string> = ko.observable<string>('');
    colorswitches: KnockoutObservableArray<any> = ko.observableArray<any>();
    beanie: KnockoutObservable<any> = ko.observable<any>();
    colors: KnockoutObservableArray<any> = ko.observableArray<any>();
    activeColorSwitch;
    buyButton: KnockoutObservable<any> = ko.observable<any>();
    bcc: KnockoutComputed<string>;
    switchPosition: KnockoutObservable<string> = ko.observable<string>();
    loc: Array<string>;

    constructor(p) {
      super(p);
      this.subscribtions();
      this.title('Color your beanie');
    }

    randomColors() {
      var colors = this.colorswitches();
      var _colors = this.colors();
      for (var i = 0; i < colors.length; i++) {
        var rc = _colors[Math.floor(Math.random() * _colors.length - 1) + 1].class;
        colors[i].color(rc);
      }
    }

    subscribtions() {
      ko.postbox.subscribe("hide:all", () => {
        if (this.visible()) {
          var _colors = [];
          var colors = this.colorswitches();
          for (var i = 0; i < colors.length; i++) {
            _colors.push(colors[i].color());
          }
          var d = this.loc;
          localStorage.setItem('beanie:'+d[0]+':'+d[1], JSON.stringify(_colors));
          this.visible(false);
        }
      });

      ko.postbox.subscribe("color", (d:Array<string>) => {
        this.visible(true);
        this.loc = d;
        try {
          this.getData(d[0], d[1]);

        } catch (e) {
          console.error(' Beanie could not be found or is misconfigured', d);
        }
      });

      this.bcc = ko.computed(() => {
        var ss = '';
        this.colorswitches().forEach((val) => {
          ss += val.group + '-' + val.color() + ' ';
        });
        return ss;
      })

    }
    selectColor(e) {
      this.activeColorSwitch.color(e);
      if(this.app.isMobile) {
          this.switchPosition(null);
      }
    }
    selectColorSwitch(e) {
      this.selectColorGroup(e);
    }
    selectColorGroup(e) {
      if (this.activeColorSwitch) {
        this.activeColorSwitch.active(false);
      }
      this.activeColorSwitch = e;
      this.activeColorSwitch.active(true);
      this.switchPosition(e.position);
    }

    order(d, e) {
      var info = d;
      var randomstring = Math.random().toString(36).slice(-8);
      console.info('order ', this, d, e);
      var f = document.createElement('form');
      f.style.display = 'none';
      e.appendChild(f);
      f.method = 'POST';
      f.action = e.href;
      var v = document.createElement('input');
      v.setAttribute('type', 'hidden');
      v.setAttribute('name', 'id');
      v.setAttribute('value', d.id);
      f.appendChild(v);
      var r = document.createElement('input');
      r.setAttribute('type', 'hidden');
      r.setAttribute('name', 'return_to');
      r.setAttribute('value', window.document.location.origin + '/thx');
      f.appendChild(r);
      info.colors = [];
      var colors = this.colorswitches();
      for (var i = 0; i < colors.length; i++) {
        info.colors.push(colors[i].color());
      }
      var lastorders = [];
      try {
        lastorders = JSON.parse(localStorage.getItem('lastorders')) || [];
      } catch (e) {

      }
      var _orderID = 'colors:' + d.id + ':' + randomstring;
      lastorders.push(_orderID)
      localStorage.setItem('lastorder', _orderID);
      localStorage.setItem('lastorders', JSON.stringify(lastorders));
      localStorage.setItem(_orderID, JSON.stringify(info));
      f.submit();

      return false;
    }

    getColorName(c:string):string {
      var _c = this.colors();
      for(var i=0;i<_c.length;i++){
        if(_c[i].class===c)
          return _c[i].name;
      }
    }

    getData(l0, l1): void {
      this.layout(l0);
      var d = this.app.data();
      var v = d.beanies[l0];
      this.colors(v.colors);
      var r = v.variations[l1];
      var lastcolors = JSON.parse(localStorage.getItem('beanie:' + l0 + ':' + l1)) || [];
      if (lastcolors.length < 1) {
        for (var i = 0; i < r.colors; i++) {
          lastcolors.push(v.colors[Math.floor(Math.random() * v.colors.length - 1) + 1].class);
        }
      }
      var _switches = [];
      for (var i = 0; i < r.colors; i++) {
        _switches.push({
          color: ko.observable(lastcolors[i]),
          active: ko.observable(),
          position: 'p' + (i + 1),
          group: 'group' + (i + 1)
        });
      }
      this.buyButton(r.shopify);
      this.beanie(r);
      this.pattern(r.pattern);
      if (r.mask)
        this.mask(r.mask);
      else
        this.mask(v.mask);
      this.bobble(r.bobble);
      this.colorswitches(_switches);
      if(!this.app.isMobile)
          this.selectColorSwitch(this.colorswitches()[0]);
    }

  }
}
