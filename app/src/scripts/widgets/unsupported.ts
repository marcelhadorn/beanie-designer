

/// <reference path="../_references.ts" />

module beanie.widgets {

  export class Unsupported extends beanie.Widget {
    title: KnockoutObservable<string> = ko.observable<string>('');
    constructor(p) {
      super(p);
      this.subscribtions();
      this.title('Unsuported');
    }

    subscribtions() {
      ko.postbox.subscribe("hide:all", () => {
        this.visible(false);
      });

      ko.postbox.subscribe("unsupported", (d) => {
        this.visible(true);

      });
    }

    save() {

    }

  }

}
