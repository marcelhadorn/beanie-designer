/// <reference path="../_references.ts" />

module beanie.widgets {

  export class Save extends beanie.Widget {
    title: KnockoutObservable<string> = ko.observable<string>('');
    constructor(p) {
      super(p);
      this.subscribtions();
      this.title('Please wait ...');
    }

    subscribtions() {
      ko.postbox.subscribe("hide:all", () => {
        this.visible(false);
      });

      ko.postbox.subscribe("save", (d) => {
        this.visible(true);
        this.save(d[0]);
      });
    }

    save(d: string) {
      var that = this;
      var lastorderIDs = JSON.parse(localStorage.getItem('lastorders'));
      if (!lastorderIDs) {
        this.title('Nothing to do here ...');
        return false;
      }

      var orderinfo = {
        beanies: [],
        shopifyCartId: d
      };
      for (var i = 0; i < lastorderIDs.length; i++) {
        orderinfo.beanies.push(JSON.parse(localStorage.getItem(lastorderIDs[i])));
      };
      $.ajax({
        url: 'https://k-nit.cloudant.com/custom-beanie-orders/' + d,
        data: JSON.stringify(orderinfo),
        contentType: "application/json",
        type: 'PUT',
        dataType: "json",
        beforeSend: function(xhr) {
          xhr.setRequestHeader("Authorization", "Basic Zm9ycmlja2Vzc2l6YXJhbmRpbGFueXNlOjdiYWUzZjUwMWIzMzBmMjU2Njk5YjY0NmZlMDg5NzIwYWUxZDYzMjE=");
        },
        success: function(resp) {
          //  localStorage.setItem(lastorderID, JSON.stringify(orderinfo));
          localStorage.removeItem('lastorders');
          that.title('Thank you for your order!');
        }
      });
    }

  }

}
