##Installation

npm i && bower i
cd src/styles
bourbon install && neat install

##Development
npm start

##Build
npm run dist


###Styles
    Format: SCSS
    Location: src/styles
    buildProcess: css: concat & minify


## Data
  The Data for this app can be found in ./src/data.json
  structure is as follows:

  ---
  title: "Beanie Designer"
  beanies:
    howitt:
      title: "Howitt"
      img: "images/howitt.jpg"
      variations:
        glory-boy:
          title: "Glory Boy"
          img: "images/gloryboy.jpg"
          patterns: []
          colors: []

    cormack:
       title: "Cormack"
       img: "images/cormack.jpg"
       variations:
         glory-boy:
           title: "Hefner"
           img: "images/gloryboy.jpg"
         haven:
           title: "Pillow"
           img: "images/haven.jpg"
         striangles:
           title: "Starspangler"
           img: "images/striangles.jpg"

###Open:
  Waiting for layout "colors" final edition.
