var watch_typescript = true;

var gulp = require('gulp');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var tsc = require('gulp-typescript-compiler');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var htmlmin = require('gulp-htmlmin');
var mainBowerFiles = require('main-bower-files');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var rimraf = require('rimraf');
var inject = require('gulp-inject');

var paths = {
  data: './src/**.json',
  dist: './dist',
  distJs: './dist/js',
  sass: ['./src/styles/**/*.scss'],
  ts: ['./src/scripts/**/*.ts'],
  html: ['./src/**/*.html']
};

gulp.task('default', ['sass', 'images', 'copy:data', 'fonts', 'ts', 'bower-files', 'html', 'browser-sync', 'watch']);

gulp.task('dist', ['build']);

gulp.task('build', ['sass', 'ts', 'images', 'copy:data', 'fonts', 'bower-files', 'html']);

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: paths.dist
        }
    });
});

gulp.task('clean', function (cb) {
    rimraf(paths.dist, cb);
});

//concat & uglify bower dependencies
gulp.task('bower-files', function() {
    return gulp.src(mainBowerFiles())
    .pipe(concat('lib.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.distJs));
});

gulp.task('html', function() {
  return gulp.src('src/index.html')
    .pipe(inject(gulp.src('./src/views/*.html'), {starttag: '<!-- inject:templates:{{ext}} -->', transform: function (filePath, file) {
      // return file contents as string
      return file.contents.toString('utf8');
    }}))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(paths.dist))
    .pipe(reload({ stream:true }));
});

gulp.task('sass', function(done) {
  gulp.src('./src/styles/index.scss')
    .pipe(sass())
    .pipe(gulp.dest('./dist/css/'))
    .pipe(reload({ stream:true }))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./dist/css/'))
    .on('end', done)
    .pipe(reload({ stream:true }));
});

// Copy images
gulp.task('images', function () {
    return gulp.src(['./src/images/**/**'])
        .pipe(gulp.dest('./dist/images'));
});

// Copy data
gulp.task('copy:data', function () {
    return gulp.src([paths.data])
      .pipe(gulp.dest('./dist/'));
});

// Copy fonts
gulp.task('fonts', function () {
    return gulp.src(['./src/fonts/**/**'])
        .pipe(gulp.dest('./dist/fonts'));
});

//Typescript
gulp.task('ts', function () {
  return gulp.src('./src/scripts/app.ts', {read: false})
    .pipe(tsc({
      resolve: true,
      module: 'commonjs',
      target: 'ES3',
      sourcemap: false,
      logErrors: true
    }))
    .pipe(concat('min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.distJs))
    .pipe(reload({ stream:true }));
});

//Watch tasks
gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.data, ['copy:data']);
  if(watch_typescript)
    gulp.watch(paths.ts, ['ts']);
});
